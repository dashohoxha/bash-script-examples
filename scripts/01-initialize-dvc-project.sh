#!/bin/bash

rm -rf proj1

set -o verbose

### Initialize a DVC project

## Create a project directory:

:; mkdir proj1

## Initialize DVC:

:; cd proj1
  
:; dvc init --no-scm

:; ls -al
  
: '
With the option --no-scm we are telling it that we are not going
to use it together with GIT.
'
  
## Check the .dvc directory that was created:

:; tree -a

:; cat .dvc/config

: It has created an empty cache directory and an empty config file.

## Create a file and track it with DVC:

:; echo 'test 1' > file1.txt

:; cat file1.txt

:; dvc add file1.txt

:; ls -l

:; tree -a

: DVC is now tracking 'file1.txt'.

## Disengage DVC:

:; dvc destroy

:; ls -al

:; tree -a

: '
Notice that the data-tracking file `file1.txt.dvc` (which tracks
`file1.txt`) is removed, while the data file itself (`file1.txt`) is
not. The DVC directory `.dvc/` is removed as well, along with the
cache, config and meta-data files.
'
