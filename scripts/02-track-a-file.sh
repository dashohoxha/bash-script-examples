#!/bin/bash

rm -rf proj1
mkdir proj1
cd proj1/
echo 'test 1' > file1.txt

set -o verbose

### Track a file

## Initialize DVC again:

:; dvc init --no-scm

:; tree -a

: "DVC doesn't have to be initialized on an empty directory (as in the previous example)."

## Start tracking file1.txt:

:; cat file1.txt

:; dvc add file1.txt

:; ls -l

:; cat file1.txt.dvc

:; md5sum file1.txt

:; tree -a

:; cat .dvc/cache/24/90a3d39b0004e4afeb517ef0ddbe2d

:; diff -u file1.txt .dvc/cache/24/90a3d39b0004e4afeb517ef0ddbe2d

: '
Do you see what happened? The file `file1.txt.dvc` that was created
contains (among other things) the path of the tracked file `path:
file1.txt` and its MD5 hash `md5: 2490a3d39b0004e4afeb517ef0ddbe2d`. A
copy of the file `file1.txt` is stored on the cache, with a name that
is composed of its MD5 hash.
'

## Remove it and restore it:

:; rm file1.txt

:; ls

:; dvc status

:; dvc checkout file1.txt.dvc

:; ls

:; cat file1.txt

: 'DVC can detect a missing data file and can help us to restore it (from cache).'

## Modify it and restore it:

:; echo 'xyz' > file1.txt

:; cat file1.txt

:; cat file1.txt.dvc

:; md5sum file1.txt

: '
As you see, the MD5 sum of the new file is different from what is
stored on the tracking file.
'

:; dvc status

: 'And sure enough DVC detects that it has been modified.'

:; dvc checkout file1.txt.dvc

:; dvc status

:; cat file1.txt

: 'It has been restored from the version that was cached.'

## To stop tracking file1.txt we can simply remove the tracking file file1.txt.dvc:

:; rm file1.txt.dvc

:; dvc status

:; ls

: '
Everything seems OK. However the cached version of the file is
still there and now it is useless:
'

:; tree -a

: 'We can clean it up like this:'

:; dvc gc

:; tree -a

:; dvc status

: '
The command `gc` means "garbage cleaner" and it removes from cache
any dangling files (that are not referenced from any data-tracking
files).
'
