#!/bin/bash
set -o verbose

### Installing DVC with pip

# Install the latest release.
:; sudo pip3 install dvc --upgrade

# Check the version.
:; dvc version

# Show the help.
:; dvc --help
