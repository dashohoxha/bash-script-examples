
# Simple bash script examples and tutorials

These scripts represent simple examples or tutorials about how to
install or configure things in command-line Linux. The best way to
learn from them is to run their commands manually, one by one, while
you also read the comments that explain the situation and what is
going on.

However, if you just run the scripts, you might also be able to follow
what is going on, since they run in the verbose mode (`set -o
verbose`), which shows the commands that are being executed, as well
as the comments.

You will have to scroll the screen in order to see what happened. Or
you may pipe them to `less` like this: `<script.sh> 2>&1 | less -r`.
You may also save the output to a text file, like this: `<script.sh>
&> log.txt`, and later view/inspect it with `less -r log.txt`.

However a better way is to play them interactively like this: `./play
<script.sh>` This will normally stop and wait for the `[Enter]` key
for each line that starts with `##*` and with `:*;*`. It will also
colorize these lines, as well as the lines that start with `#*` and
`:*`.

If you want the script to autoplay, you can use the environment
variable `DELAY`, like this: `DELAY=0.5 ./play <script.sh>` Then the
script will not wait for `[Enter]` anymore but will just sleep the
given amount of time and then continue automatically.

The commands `highlight` and `unbuffer` are dependencies, which can be
installed like this: `apt install highlight expect`
