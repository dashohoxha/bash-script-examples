#!/bin/bash -i

# initialize
rm -rf cryptsetup-tutorial
mkdir -p cryptsetup-tutorial
cd cryptsetup-tutorial/

set -o verbose

# In this tutorial we will see how to create and use an encrypted
# archive with *cryptsetup*. This might be useful if you keep personal
# data on a movable device (for example a USB stick), which might be
# lost or accessed by unauthorized people. In this case they won't be
# able to look at your personal data without knowing the password.
##

### Step1: Create the archive

## 1. Make sure that `cryptsetup` is installed:

:; apt list cryptsetup
   
:; apt install cryptsetup
   
## 2. Create an image file for the archive:

:; fallocate -l 15G archive1.img
   
:; ls -lh
   
# However, to make sure that the created file has random data,
# you can use a command like this (which is slower):
##

:; dd if=/dev/urandom of=archive1.img bs=10M count=100
   
# This would increase the encryption strength, since an attacker
# can't distinguish the part of the archive that is free from the
# part that has encrypted data.
##

## 3. Create a loop device for this image file:

:; losetup -f archive1.img
   
:; losetup -a
   
:; losetup -a | grep archive1.img
   
:; lsblk
   
:; lsblk | grep loop

### Step2: Format the archive

## 1. Using the `cryptsetup` command, format the drive with Linux Unified Key Setup (LUKS):

:; cryptsetup luksFormat /dev/loop0
   
# LUKS stores some metadata at the beginning of the partition
# regarding the type of encryption used and the encryption key. The
# key is randomly generated but is itself encrypted using a
# passphrase that you provide. The passphrase should be at least 3
# random words.
##

## 2. Create a virtual device using `cryptsetup` that encrypts and decrypts all data going to and from the loop device:
   

:; cryptsetup luksOpen /dev/loop0 archive1
   
:; lsblk

:; lsblk | grep crypt

:; ls /dev/mapper/
   
:; ls -l /dev/mapper/archive1
   
## 3. Format the virtual device node `/dev/mapper/archive1` with ext4:

:; mkfs.ext4 /dev/mapper/archive1 -L archive1
   
# The whole ext4 filesystem that is created will be encrypted.
##

## 4. Close the virtual device:

:; umount /dev/mapper/archive1

:; cryptsetup luksClose archive1
   
:; ls /dev/mapper/
   
:; lsblk
   
:; losetup -a
   
:; losetup -d /dev/loop0
   
:; losetup -a
   
:; lsblk

### Step 3: Open the archive

## 1. Attach it to a loop device:

:; losetup -f archive1.img
   
:; losetup -a
   
:; losetup -a | grep archive1.img
   
:; lsblk
   
## 2. Create an encrypted virtual device:

:; cryptsetup luksOpen /dev/loop0 archive1
   
:; ls /dev/mapper/
   
:; lsblk
   
## 3. Mount the encrypted device:

:; mkdir -p archive1
   
:; mount /dev/mapper/archive1 archive1
   
:; ls
   
:; df -h

:; df -h archive1/

## 4. Create a file in it:

:; touch archive1/personal_data
   
:; ls -al archive1/


### Step 4: Close the archive
   
## 1. Unmount the encrypted device:

:; umount /dev/mapper/archive1
   
:; df -h
   
:; ls -al archive1/
   
:; lsblk
   
:; ls /dev/mapper/

# Optionally, remove the directory as well:
##

:; rmdir archive1/
   
## 2. Close the encrypted device:
   
:; cryptsetup luksClose archive1
   
:; ls /dev/mapper/
   
:; lsblk
   
## 3. Optionally, remove the loopback device as well:

:; losetup -a | grep archive1.img
   
:; losetup -d /dev/loop0
   
:; losetup -a
   
:; lsblk

### Step 5: Using a script

# The script on https://gitlab.com/snippets/1943582 can help to create,
# mount and unmount an encrypted archive.
##
   
## 1. Get it:

:; wget -q -O archive.sh https://gitlab.com/snippets/1943582/raw
   
:; ls -lh
   
:; less archive.sh
   
:; chmod +x archive.sh
   
:; ls -lh
   
## 2. Create a new archive:
   
:; ./archive.sh create archive2.img
   
:; ls -lh
   
## 3. Mount this archive:

:; ./archive.sh mount archive2.img
   
:; ls -lh
   
:; lsblk
   
:; df -h
   
:; df -h archive2/
   
:; ls /dev/mapper/

## 4. Unmount it:

:; ./archive.sh unmount archive2.img
   
:; ls -lh
   
:; lsblk
   
:; df -h
   
:; ls /dev/mapper/
